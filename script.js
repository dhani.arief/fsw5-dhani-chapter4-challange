let player = 0;
let komputer = 0;
const playerScore = document.getElementById("player-score");
const komputerScore = document.getElementById("computer-score");
const batu = document.getElementById("batu-player")
const kertas = document.getElementById("kertas-player")
const gunting = document.getElementById("gunting-player")
const result = document.getElementById("VS");
const refresh = document.getElementById("refresh")

function ambilPilihanKomputer(){
    const pilihan = ["batu", "kertas","gunting"];
    return pilihan[Math.floor(Math.random() * pilihan.length)]
}

function win() {
    player++;
    playerScore.innerHTML = player;
    komputerScore.innerHTML = komputer;
    result.src = "assets/player-win.png";
}


function lose() {
    komputer++;
    komputerScore.innerHTML = komputer;
    playerScore.innerHTML = player;
    result.src = "assets/com-win.png";
}

function draw() {
    result.src = "assets/draw.png";
}

function reset() {
    player = 0;
    komputer = 0;
    playerScore.innerHTML = player;
    komputerScore.innerHTML = komputer;
    result.src = "assets/VS.png";
}

function game(pilihanPlayer) {
    const pilihanKomputer = ambilPilihanKomputer();
    console.log("pilihan user = " +pilihanPlayer)
    console.log("pilihan komputer = " + pilihanKomputer)
    switch (pilihanPlayer + pilihanKomputer) {
        case "batugunting":
        case "guntingkertas":
        case "kertasbatu":
            win();
            break;
        case "batukertas":
        case "kertasgunting":
        case "guntingbatu":
            lose();
            break;
        case "batubatu":
        case "kertaskertas":
        case "guntinggunting":
            draw();
            break;
}
}

function main(){
batu.addEventListener("click", function(){
    game("batu");
})

kertas.addEventListener("click", function(){
    game("kertas");
})

gunting.addEventListener("click", function(){
    game("gunting");
})

refresh.addEventListener("click", function(){
    reset("refresh");
})
}

main();